---
name: 'Meizu Pro 5'
comment: 'legacy'
deviceType: 'phone'
enableMdRendering: true
image: 'https://topcom.lt/wp-content/uploads/2016/02/Meizu-Pro-5-Ubuntu-Edition.jpg'
maturity: .9
---

Meizu Pro 5 devices that are sold with Android have a locked bootloader, so installing UBports' version of Ubuntu Touch is only possible on devices that come with Ubuntu in the first place.
