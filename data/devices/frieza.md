---
name: 'Bq Aquaris M10 FHD'
deviceType: 'tablet'
enableMdRendering: true
portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "+"
      - id: "torchlight"
        value: "x"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "x"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "x"
      - id: "dataConnection"
        value: "x"
      - id: "calls"
        value: "x"
      - id: "mms"
        value: "x"
      - id: "pinUnlock"
        value: "x"
      - id: "sms"
        value: "x"
      - id: "audioRoutings"
        value: "x"
      - id: "voiceCall"
        value: "x"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "+"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "+"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "x"
      - id: "wirelessExternalMonitor"
        value: "+"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "x"
      - id: "nfc"
        value: "x"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+"
      - id: "fingerprint"
        value: "x"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "x"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+"
      - id: "adb"
        value: "+"
      - id: "wiredExternalMonitor"
        value: "+"
deviceInfo:
  - id: "cpu"
    value: "Quad-Core Cortex-A53 1.5Ghz"
  - id: "chipset"
    value: "MediaTek MT8163A"
  - id: "gpu"
    value: "Mali-T720MP2"
  - id: "rom"
    value: "16GB"
  - id: "ram"
    value: "2GB"
  - id: "android"
    value: "Android 5.1"
  - id: "battery"
    value: "7280 mAh"
  - id: "display"
    value: "1920x1200 pixels, 10.1 in"
  - id: "rearCamera"
    value: "8MP"
  - id: "frontCamera"
    value: "5MP"
externalLinks:
  - name: "Telegram"
    link: "https://t.me/joinchat/ubports"
    icon: "telegram"
  - name: "Device Subforum"
    link: "https://forums.ubports.com/category/75/bq-m10-hd"
    icon: "yumi"
  - name: "Report a bug"
    link: "https://github.com/ubports/ubuntu-touch/issues"
    icon: "github"
---

BQ M10 FHD devices that are sold with Android have a locked bootloader, so those need to be [manually unlocked by installing the manufacturer's Ubuntu image](https://docs.ubports.com/en/latest/userguide/install.html#install-on-legacy-android-devices) before switching to UBports' release of Ubuntu Touch.
