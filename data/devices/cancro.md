---
name: 'Xiaomi 4'
deviceType: 'phone'
installLink: 'https://gitlab.com/ubports/community-ports/cancro'
maturity: 0

externalLinks:
  -
    name: 'Repository'
    link: 'https://gitlab.com/ubports/community-ports/cancro'
    icon: 'github'

seo:
  description: 'Switch your Xiaomi 4 OS to Ubuntu Touch, as your daily driver, a privacy focused OS.'
  keywords: 'Ubuntu Touch, Xiaomi 4, linux for smartphone, Linux on Phone'

---
