module.exports = {
  run: elaborate,
  clean: clean
};

const portStatus = require("../../data/portStatus.json");
const deviceInfo = require("../../data/deviceInfo.json");
const progressStages = require("../../data/progressStages.json");

const slugify = require("@sindresorhus/slugify");
const dataUtils = require("./dataUtils.js");

function elaborate(device, installerData) {
  // Set codename from file name
  device.codename = device.fileInfo.name;

  if (device.portStatus) {
    addMissingFeatures(device);
    addGlobalState(device);
    deleteUnavailableFeatures(device);
  }

  getInstallerSupport(device, installerData);
  calculateProgress(device);
  calculateProgressStage(device);

  addDeveloperReport(device);
}

function clean(device, installerData) {
  if (device.portStatus) {
    useNameIsteadOfFeatureId(device);
    deleteVoidCategories(device);
  }

  if (device.deviceInfo) {
    useNameIsteadOfSpecificationId(device);
  }
}

/* Functions */

// Add missing features from defaults
function addMissingFeatures(device) {
  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (!graphQlFeature) {
      // Don't add unavailable features
      if (feature.default != "x") {
        category.features.push({
          name: feature.name,
          value: feature.default ? feature.default : "?"
        });
      }
    }
  });
}

// Check global state of the feature
function addGlobalState(device) {
  let featureScale = ["x", "-", "?", "+-", "+"];

  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (
      graphQlFeature &&
      feature.global &&
      featureScale.indexOf(feature.global) <=
        featureScale.indexOf(graphQlFeature.value)
    ) {
      graphQlFeature.value = feature.global;
      graphQlFeature.bugTracker = feature.bugTracker ? feature.bugTracker : "";
      graphQlFeature.global = true;
    }
  });
}

// Delete unavailable features
function deleteUnavailableFeatures(device) {
  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (graphQlFeature && graphQlFeature.value == "x") {
      category.features.splice(category.features.indexOf(graphQlFeature), 1);
    }
  });
}

// Get installer compatibility data
function getInstallerSupport(device, installerData) {
  let deviceCodenameAlias = device.installerAlias
    ? device.installerAlias
    : device.codename;
  let deviceInstaller = installerData.some(
    (el) =>
      el.codename == deviceCodenameAlias &&
      el.operating_systems.includes("Ubuntu Touch")
  );
  device.noInstall = !deviceInstaller;
}

// Calculate porting progress from feature matrix ( use maturity field as fallback )
function calculateProgress(device) {
  if (device.portStatus) {
    let totalWeight = 0,
      currentWeight = 0;

    dataUtils.forEachFeature(device, function (feature, category) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

      if (graphQlFeature) {
        totalWeight += feature.weight;
        currentWeight += graphQlFeature.value == "+" ? feature.weight : 0;
      }
    });

    device.progress = currentWeight / totalWeight;
  } else {
    // Fallback from maturity
    device.progress = device.maturity;
  }

  // Add weight to installer and port data
  device.progress = device.noInstall ? device.progress * 0.9 : device.progress;
  device.progress = device.portStatus
    ? device.progress
    : device.progress * 0.95;
  device.progress = Math.round(1000 * device.progress) / 10;
}

// Calculate progress stage
function calculateProgressStage(device) {
  if (device.portStatus) {
    let currentStageIndex = progressStages.length - 1; // Daily-driver ready

    dataUtils.forEachFeature(device, function (feature, category) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

      if (graphQlFeature && graphQlFeature.value != "+") {
        let featureStageIndex = progressStages.indexOf(feature.stage);
        if (currentStageIndex >= featureStageIndex && featureStageIndex > 0) {
          currentStageIndex = featureStageIndex - 1;
        }
      }
    });

    device.progressStage = progressStages[currentStageIndex];
  } else {
    // Fallback from maturity
    device.progressStage = progressStages[0];
  }
}

// Add developer report to externalLinks
function addDeveloperReport(device) {
  let devReportLink = {
    name: "Developer report",
    link:
      "/device/" +
      slugify(device.codename, { decamelize: false }) +
      "/developer",
    icon: "yumi"
  };

  if (device.externalLinks) {
    device.externalLinks.push(devReportLink);
  } else {
    device.externalLinks = [devReportLink];
  }
}

// Replace ID with name
function useNameIsteadOfFeatureId(device) {
  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (graphQlFeature) {
      graphQlFeature.name = feature.name;
      delete graphQlFeature.id;
    }
  });
}

// Delete categories that have no features set
function deleteVoidCategories(device) {
  for (let portCategory in portStatus) {
    let category = dataUtils.getCategoryByName(device, portCategory);

    if (category && category.features.length == 0) {
      device.portStatus.splice(device.portStatus.indexOf(category), 1);
    }
  }
}

// Elaborate device info from IDs
function useNameIsteadOfSpecificationId(device) {
  device.deviceInfo.forEach((el) => {
    el.name = deviceInfo.find((info) => info.id == el.id).name;
    delete el.id;
  });
}
